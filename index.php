<!DOCTYPE html>

<html lang="en">
  <head>
    
    <!-- global css-->
    <?php include('theme_meta_tag.php') ?>
    <!-- end global css-->
    <title><?="Dashboard | ASET KITA"?></title>
    <!-- global css-->
    <?php include('theme_global_css.php') ?>
    <!-- end global css-->
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <!-- header -->
    <?php include('theme_header.php') ?>
    <!-- end header -->
    <div class="app-body">
    <!-- sidebar -->
    <?php include('theme_sidebar.php') ?>
    <!-- end sidebar -->
      <main class="main">
        <!-- Breadcrumb-->
        <?php include('theme_breadcrumb.php'); ?>
        <!-- end breadcumb -->
        <div class="container-fluid">
          <!-- content -->
          <?php include('theme_content.php'); ?>
          <!-- end content -->
        </div>
      </main>
    </div>
    <!-- sidebar -->
    <?php include('theme_footer.php') ?>
    <!-- end sidebar -->

    <!-- CoreUI and necessary plugins-->
    <?php include('theme_global_js.php'); ?>
  </body>
</html>
