<footer class="app-footer">
      <div>
        <a href="javascript:;">Aset Kita</a>
        <span>&copy; 2019 PJJ AK C.</span>
      </div>
      <div class="ml-auto">
        <span>Made with <i class="fa fa-heart" style="color:#eb4d4b;"></i> In Surabaya</span>
      </div>
    </footer>