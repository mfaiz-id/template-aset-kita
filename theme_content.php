<div class="animated fadeIn">
    <div class="row animated fadeInUp">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-primary">
                <div class="card-body pb-0">
                    <div class="btn-group float-right">
                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <div class="text-value">9.823</div>
                    <div>Members online</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-info">
                <div class="card-body pb-0">
                    <button class="btn btn-transparent p-0 float-right" type="button">
                        <i class="icon-location-pin"></i>
                    </button>
                    <div class="text-value">9.823</div>
                    <div>Members online</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart2" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-warning">
                <div class="card-body pb-0">
                    <div class="btn-group float-right">
                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <div class="text-value">9.823</div>
                    <div>Members online</div>
                </div>
                <div class="chart-wrapper mt-3" style="height:70px;">
                    <canvas class="chart" id="card-chart3" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-danger">
                <div class="card-body pb-0">
                    <div class="btn-group float-right">
                        <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <div class="text-value">9.823</div>
                    <div>Members online</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart4" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    <div class="row animated fadeInUp">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>Contoh Form</strong>
                    <small>Form</small>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control" id="name" type="text" placeholder="Enter your name">
                            </div>
                        </div>
                    </div>
                    <!-- /.row-->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="ccnumber">Credit Card Number</label>
                                <input class="form-control" id="ccnumber" type="text" placeholder="0000 0000 0000 0000">
                            </div>
                        </div>
                    </div>
                    <!-- /.row-->
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="ccmonth">Month</label>
                            <select class="form-control" id="ccmonth">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="ccyear">Year</label>
                            <select class="form-control" id="ccyear">
                                <option>2014</option>
                                <option>2015</option>
                                <option>2016</option>
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                                <option>2023</option>
                                <option>2024</option>
                                <option>2025</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="cvv">CVV/CVC</label>
                                <input class="form-control" id="cvv" type="text" placeholder="123">
                            </div>
                        </div>
                    </div>
                    <!-- /.row-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>Contoh Buttons</strong>
                </div>
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-12 col-xl mb-3 mb-xl-0">Normal</div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-primary" type="button">Primary</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-secondary" type="button">Secondary</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-success" type="button">Success</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-warning" type="button">Warning</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-danger" type="button">Danger</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-info" type="button">Info</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-light" type="button">Light</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-dark" type="button">Dark</button>
                        </div>
                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                            <button class="btn btn-block btn-link" type="button">Link</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row animated fadeInUp">
        <div class="col-md-6">
        <div class="card">
                <div class="card-header">
                    <strong>Contoh Alert</strong>
                </div>
                <div class="card-body">
                    <button onclick="sweet_biasa()" class="btn btn-primary">Alert Success</button>
                    <button onclick="konfirmasi()" class="btn btn-primary">Konfirmasi</button>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function sweet_biasa(){
        Swal.fire(
            'Good job!',
            'You clicked the button!',
            'success'
        );
    }
    function konfirmasi() {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
                )
            }
        });
    }
</script>