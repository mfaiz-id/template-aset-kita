<link rel="icon" type="image/ico" href="assets/template/img/favicon.png" sizes="any" />
<link href="node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
<link href="node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
<link href="node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
<!-- Main styles for this application-->
<link href="assets/template/css/style.css" rel="stylesheet">
<link href="assets/template/css/animate.css" rel="stylesheet">
<link href="vendors/pace-progress/css/pace.min.css" rel="stylesheet">
<script src="node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="node_modules/sweetalert2/dist/sweetalert2.min.css">